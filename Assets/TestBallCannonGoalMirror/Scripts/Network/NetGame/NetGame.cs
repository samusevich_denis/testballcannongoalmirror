using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System;
using UnityEngine.SceneManagement;

namespace TestBallCannonGoalMirror
{
    public class NetGame : NetworkBehaviour
    {
        public event Action<int, int, int> OnPlayerIndexChangedEvent;
        public event Action<int, int, int> OnPlayerColorChangedEvent;
        public event Action<int, bool, bool> OnEnterGameBlockChangedEvent;
        public event Action<int, bool, bool> OnPlayerReadyStateChangedEvent;
        public event Action<bool, bool> OnGameStateChangedEvent;
        public event Action<int> OnAddScoreForPlayer;
        public event Action<int> OnPlayerExitToLobby;
        public Scene GameScene { get; set; }
        [SyncVar(hook = nameof(EnterGameBlockChanged))] public bool IsCanEnterGame;
        [SyncVar(hook = nameof(IndexChanged))] public int GameIndex;
        [SyncVar(hook = nameof(GameStateChanged))] public bool IsStartGame;
        [SyncVar(hook = nameof(Player_00_IndexChanged))] public int Player_00;
        [SyncVar(hook = nameof(Player_00_ColorChanged))] public int Player_00_Color;
        [SyncVar(hook = nameof(Player_00_ReadyStateChanged))] public bool Player_00_IsReady;
        [SyncVar(hook = nameof(Player_01_IndexChanged))] public int Player_01;
        [SyncVar(hook = nameof(Player_01_ColorChanged))] public int Player_01_Color;
        [SyncVar(hook = nameof(Player_01_ReadyStateChanged))] public bool Player_01_IsReady;
        [SyncVar(hook = nameof(Player_02_IndexChanged))] public int Player_02;
        [SyncVar(hook = nameof(Player_02_ColorChanged))] public int Player_02_Color;
        [SyncVar(hook = nameof(Player_02_ReadyStateChanged))] public bool Player_02_IsReady;
        [SyncVar(hook = nameof(Player_03_IndexChanged))] public int Player_03;
        [SyncVar(hook = nameof(Player_03_ColorChanged))] public int Player_03_Color;
        [SyncVar(hook = nameof(Player_03_ReadyStateChanged))] public bool Player_03_IsReady;
        private bool isGameFull;
        private NetworkLobbyManager networkLobbyManager;
        private List<int> colorPool = new List<int>(); 

        #region Unity Callbacks
        void Start()
        {
            if (NetworkManager.singleton is NetworkLobbyManager lobby)
            {
                networkLobbyManager = lobby;
                if (lobby.dontDestroyOnLoad)
                    DontDestroyOnLoad(gameObject);

                if (NetworkServer.active)
                {
                    IsCanEnterGame = true;
                    GameIndex = lobby.NextGameIndex;
                    colorPool.Add(1);
                    colorPool.Add(2);
                    colorPool.Add(3);
                    colorPool.Add(4);
                    colorPool.Add(5);
                }
            }
            else Debug.LogError("NetGame could not find a NetworkLobbyManager. The NetLobbyPlayer requires a NetworkLobbyManager object to function. Make sure that there is one in the scene.");
        }

        private void OnDestroy()
        {
            GamesList.RemoveNetGame(this);
        }

        #endregion

        [Command(requiresAuthority = false)]
        public void ChangePlayerColor(int playerIndex)
        {
            if (Player_00 == playerIndex)
            {
                Player_00_Color = GetNewColor(Player_00_Color);
            }
            else if (Player_01 == playerIndex)
            {
                Player_01_Color = GetNewColor(Player_01_Color);
            }
            else if (Player_02 == playerIndex)
            {
                Player_02_Color = GetNewColor(Player_02_Color);
            }
            else if (Player_03 == playerIndex)
            {
                Player_03_Color = GetNewColor(Player_03_Color);
            }
        }

        [Server]
        public void AddScoreForPlayer(int indexPlayer)
        {
            OnAddScoreForPlayer?.Invoke(indexPlayer);
        }

        [ClientRpc]
        public void PlayerExitToLobby(int playerIndex)
        {
            OnPlayerExitToLobby?.Invoke(playerIndex);
        }

        [Command(requiresAuthority = false)]
        public void CmdSetPlayerReadyState(int playerIndex, bool value)
        {
            if (Player_00 == playerIndex)
            {
                Player_00_IsReady = value;
            }
            else if (Player_01 == playerIndex)
            {
                Player_01_IsReady = value;
            }
            else if (Player_02 == playerIndex)
            {
                Player_02_IsReady = value;
            }
            else if (Player_03 == playerIndex)
            {
                Player_03_IsReady = value;
            }
            if (IsCanEnterGame && IsPlayerReady(Player_00, Player_00_IsReady)
                && IsPlayerReady(Player_01, Player_01_IsReady)
                && IsPlayerReady(Player_02, Player_02_IsReady)
                && IsPlayerReady(Player_03, Player_03_IsReady))
            {
                IsStartGame = true;
                networkLobbyManager.StartFotballGame(this);
                IsCanEnterGame = false;
            }
            else if (IsCanEnterGame == false && (IsPlayerReady(Player_00, Player_00_IsReady) == false
                || IsPlayerReady(Player_01, Player_01_IsReady) == false
                || IsPlayerReady(Player_02, Player_02_IsReady) == false
                || IsPlayerReady(Player_03, Player_03_IsReady) == false))
            {
                IsCanEnterGame = isGameFull ? false : true;
            }

            bool IsPlayerReady(int indexPlayer, bool readyState)
            {
                return indexPlayer > 0 && readyState || indexPlayer == 0;
            }
        }

        [Command(requiresAuthority = false)]
        public void CmdSetPlayer(int playerIndex)
        {
            if (Player_00 == 0)
            {
                Player_00 = playerIndex;
                Player_00_Color = GetNewColor(Player_00_Color);
            }
            else if (Player_01 == 0)
            {
                Player_01 = playerIndex;
                Player_01_Color = GetNewColor(Player_01_Color);
            }
            else if (Player_02 == 0)
            {
                Player_02 = playerIndex;
                Player_02_Color = GetNewColor(Player_02_Color);
            }
            else if (Player_03 == 0)
            { 
                Player_03 = playerIndex;
                Player_03_Color = GetNewColor(Player_03_Color);
            }
            if (IsCanEnterGame == false && Player_00 > 0 && Player_01 > 0 && Player_02 > 0 && Player_03 > 0)
            {
                isGameFull = true;
                IsCanEnterGame = false;
            }
            Player_00_IsReady = false;
            Player_01_IsReady = false;
            Player_02_IsReady = false;
            Player_03_IsReady = false;
        }

        [Command(requiresAuthority = false)]
        public void CmdRemovePlayer(int playerIndex)
        {
            if (Player_00 == playerIndex)
            {
                Player_00 = 0;
                Player_00_Color = ReturnColor(Player_00_Color);

            }
            else if (Player_01 == playerIndex)
            {
                Player_01 = 0;
                Player_01_Color = ReturnColor(Player_01_Color);
            }
            else if (Player_02 == playerIndex)
            {
                Player_02 = 0;
                Player_02_Color = ReturnColor(Player_02_Color);
            }
            else if (Player_03 == playerIndex)
            {
                Player_03 = 0;
                Player_03_Color = ReturnColor(Player_03_Color);
            }
            if (IsStartGame)
            {
                if (PlayersList.TryGetNetLobbyPlayerAnIndex(playerIndex, out NetLobbyPlayer lobbyPlayer))
                {
                    lobbyPlayer.connectionToClient.Send(new SceneMessage { sceneName = GameScene.name, sceneOperation = SceneOperation.UnloadAdditive });
                }
            }
            if (Player_00 == 0 && Player_01 == 0 && Player_02 == 0 && Player_03 == 0)
            {
                if (IsStartGame)
                {
                    Debug.Log("NetworkServer.Destroy NetGame");
                    NetworkServer.Destroy(this.gameObject);
                    SceneManager.UnloadSceneAsync(GameScene);
                }
                else
                {
                    Debug.Log("NetworkServer.Destroy NetGame");
                    NetworkServer.Destroy(this.gameObject);
                }
            }
            if (!IsCanEnterGame && IsStartGame == false)
            {
                IsCanEnterGame = true;
            }
            Player_00_IsReady = false;
            Player_01_IsReady = false;
            Player_02_IsReady = false;
            Player_03_IsReady = false;
        }

        private int GetNewColor(int lastColor)
        {
            var newColor = colorPool[0];
            colorPool.RemoveAt(0);
            if (lastColor!= 0)
            {
                colorPool.Add(lastColor);
            }
            return newColor;
        }

        private int ReturnColor(int lastColor)
        {
            if (lastColor != 0)
            {
                colorPool.Add(lastColor);
            }
            return 0;
        }


        #region Start & Stop Callbacks

        /// <summary>
        /// This is invoked for NetworkBehaviour objects when they become active on the server.
        /// <para>This could be triggered by NetworkServer.Listen() for objects in the scene, or by NetworkServer.Spawn() for objects that are dynamically created.</para>
        /// <para>This will be called for objects on a "host" as well as for object on a dedicated server.</para>
        /// </summary>
        public override void OnStartServer() { }

        /// <summary>
        /// Invoked on the server when the object is unspawned
        /// <para>Useful for saving object data in persistent storage</para>
        /// </summary>
        public override void OnStopServer() { }



        /// <summary>
        /// Called on every NetworkBehaviour when it is activated on a client.
        /// <para>Objects on the host have this function called, as there is a local client on the host. The values of SyncVars on object are guaranteed to be initialized correctly with the latest state from the server when this function is called on the client.</para>
        /// </summary>
        public override void OnStartClient() 
        {
            
        }

        /// <summary>
        /// This is invoked on clients when the server has caused this object to be destroyed.
        /// <para>This can be used as a hook to invoke effects or do client specific cleanup.</para>
        /// </summary>
        public override void OnStopClient() 
        {
            
        }

        /// <summary>
        /// Called when the local player object has been set up.
        /// <para>This happens after OnStartClient(), as it is triggered by an ownership message from the server. This is an appropriate place to activate components or functionality that should only be active for the local player, such as cameras and input.</para>
        /// </summary>
        public override void OnStartLocalPlayer() { }

        /// <summary>
        /// Called when the local player object is being stopped.
        /// <para>This happens before OnStopClient(), as it may be triggered by an ownership message from the server, or because the player object is being destroyed. This is an appropriate place to deactivate components or functionality that should only be active for the local player, such as cameras and input.</para>
        /// </summary>
        public override void OnStopLocalPlayer() { }

        /// <summary>
        /// This is invoked on behaviours that have authority, based on context and <see cref="NetworkIdentity.hasAuthority">NetworkIdentity.hasAuthority</see>.
        /// <para>This is called after <see cref="OnStartServer">OnStartServer</see> and before <see cref="OnStartClient">OnStartClient.</see></para>
        /// <para>When <see cref="NetworkIdentity.AssignClientAuthority">AssignClientAuthority</see> is called on the server, this will be called on the client that owns the object. When an object is spawned with <see cref="NetworkServer.Spawn">NetworkServer.Spawn</see> with a NetworkConnectionToClient parameter included, this will be called on the client that owns the object.</para>
        /// </summary>
        public override void OnStartAuthority() { }

        /// <summary>
        /// This is invoked on behaviours when authority is removed.
        /// <para>When NetworkIdentity.RemoveClientAuthority is called on the server, this will be called on the client that owns the object.</para>
        /// </summary>
        public override void OnStopAuthority() { }

        #endregion

        #region SyncVar Hooks

        public virtual void IndexChanged(int oldIndex, int newIndex)
        {
            if (NetworkClient.active)
                GamesList.AddNetGame(this);
        }

        public virtual void EnterGameBlockChanged(bool oldValue, bool newValue) => OnEnterGameBlockChangedEvent?.Invoke(GameIndex, oldValue, newValue);
       
        public virtual void GameStateChanged(bool oldState, bool newState) => OnGameStateChangedEvent?.Invoke(oldState, newState);

        public virtual void Player_00_IndexChanged(int oldIndex, int newIndex) => OnPlayerIndexChangedEvent?.Invoke(0, oldIndex, newIndex);
        
        public virtual void Player_00_ColorChanged(int oldIndex, int newIndex) => OnPlayerColorChangedEvent?.Invoke(0, oldIndex, newIndex);
        
        public virtual void Player_00_ReadyStateChanged(bool oldReadyState, bool newReadyState) => OnPlayerReadyStateChangedEvent?.Invoke(0, oldReadyState, newReadyState);

        public virtual void Player_01_IndexChanged(int oldIndex, int newIndex) => OnPlayerIndexChangedEvent?.Invoke(1, oldIndex, newIndex);

        public virtual void Player_01_ColorChanged(int oldIndex, int newIndex) => OnPlayerColorChangedEvent?.Invoke(1, oldIndex, newIndex);
        
        public virtual void Player_01_ReadyStateChanged(bool oldReadyState, bool newReadyState) => OnPlayerReadyStateChangedEvent?.Invoke(1, oldReadyState, newReadyState);
        
        public virtual void Player_02_IndexChanged(int oldIndex, int newIndex) => OnPlayerIndexChangedEvent?.Invoke(2, oldIndex, newIndex);

        public virtual void Player_02_ColorChanged(int oldIndex, int newIndex) => OnPlayerColorChangedEvent?.Invoke(2, oldIndex, newIndex);
        
        public virtual void Player_02_ReadyStateChanged(bool oldReadyState, bool newReadyState) => OnPlayerReadyStateChangedEvent?.Invoke(2, oldReadyState, newReadyState);
        
        public virtual void Player_03_IndexChanged(int oldIndex, int newIndex) => OnPlayerIndexChangedEvent?.Invoke(3, oldIndex, newIndex);

        public virtual void Player_03_ColorChanged(int oldIndex, int newIndex) => OnPlayerColorChangedEvent?.Invoke(3, oldIndex, newIndex);
        
        public virtual void Player_03_ReadyStateChanged(bool oldReadyState, bool newReadyState) => OnPlayerReadyStateChangedEvent?.Invoke(3, oldReadyState, newReadyState);
        
        public virtual void ReadyStateChanged(bool oldReadyState, bool newReadyState) { }

        #endregion
    }
}