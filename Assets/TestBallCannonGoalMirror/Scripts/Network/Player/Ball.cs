using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

namespace TestBallCannonGoalMirror
{
    public class Ball : NetworkBehaviour
    {
        public int IndexPlayer;
        public Rigidbody Body;

        [ClientRpc]        
        public void SetActive(bool value)
        {
            gameObject.SetActive(value);
        }
    }
}