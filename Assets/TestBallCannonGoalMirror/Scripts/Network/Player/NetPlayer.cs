using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using Mirror;
using System;
using UnityEngine.SceneManagement;
using TMPro;
using TestBallCannonGoalMirror.Extension;
using UnityEngine.UI;

namespace TestBallCannonGoalMirror
{
    public class NetPlayer : NetworkBehaviour
    {
        public event Action<int, int> OnPlayerIndexChangedEvent;
        public event Action<int, int> OnPlayerColorChangedEvent;

        [SyncVar(hook = nameof(PlayerIndexChanged))] public int PlayerIndex;
        [SyncVar(hook = nameof(PlayerColorChanged))] public int PlayerColor;
        [SyncVar(hook = nameof(PlayerScoreChanged))] public int PlayerScore;
        [SerializeField] private Camera playerCamera;
        [SerializeField] private GameObject canvasUI;
        [SerializeField] private CannonRotating cannonRotating;
        [SerializeField] private GameObject ballPrefab;
        [SerializeField] private TextMeshPro score;
        [SerializeField] private TextMeshProUGUI scoreUI;
        [SerializeField] private int maxBall;
        [SerializeField] private Transform axisForwardCannon;
        [SerializeField] private float timeToMaxForce = 4f;
        [SerializeField] private float maxForce;
        [SerializeField] private Button exitGame;
        private List<Ball> balls = new List<Ball>();
        private NetGame game;
        private bool isPreparationShootingState;
        private float forceShot;
        private float timer;


        #region Unity Callbacks

        void Start()
        {
            if (!isOwned)
            {
                playerCamera.gameObject.SetActive(false);
                canvasUI.gameObject.SetActive(false);
            }
            else
            {
                scoreUI.color = PlayerColor.ToColor();
                exitGame.onClick.AddListener(ExitGame);
            }
        }

        private void Update()
        {
            if (!isOwned)
            {
                return;
            }
            RotateLogic();
            ShotLogic();
        }

        #endregion

        [Server]
        public void SetGameDataAndColor(NetGame _game, int index, int color)
        {
            game = _game;
            game.OnAddScoreForPlayer += AddScoreForPlayer;
            PlayerIndex = index;
            PlayerColor = color;
        }

        [Server]
        public void DestroyBall(GameObject obj)
        {
            var ball = obj.GetComponent<Ball>();
            if (ball.IndexPlayer != PlayerIndex)
            {
                PlayerScore--;
                game.AddScoreForPlayer(ball.IndexPlayer);
            }
            ball.SetActive(false);
        }

        [Command]
        private void CmdShot(float forceShot)
        {
            StartCoroutine(Shot(forceShot));
        }

        private IEnumerator Shot(float forceShot)
        {
            if (balls.Count > maxBall)
            {
                var ball = balls.GetFreeBall();
                ball.SetActive(true);
                ball.Body.velocity = Vector3.zero;
                ball.Body.angularVelocity = Vector3.zero;
                ball.Body.WakeUp();
                ball.transform.position = axisForwardCannon.position;
                ball.transform.rotation = axisForwardCannon.rotation;
                yield return new WaitForFixedUpdate();
                Debug.Log("Shot");
                ball.Body.AddForce(forceShot * axisForwardCannon.forward, ForceMode.Impulse);
            }
            else
            {
                GameObject newBall = Instantiate(ballPrefab, axisForwardCannon.position, axisForwardCannon.rotation);
                var ball = newBall.GetComponent<Ball>();
                balls.Add(ball);
                ball.Body.isKinematic = false;
                ball.IndexPlayer = PlayerIndex;
                SceneManager.MoveGameObjectToScene(newBall, game.GameScene);
                NetworkServer.Spawn(newBall);
                Debug.Log("Shot");
                ball.Body.AddForce(forceShot * axisForwardCannon.forward, ForceMode.Impulse);
            }
        }

        private void RotateLogic()
        {
            cannonRotating.RotateAroundX(Input.mousePosition.y);
            cannonRotating.RotateAroundY(Input.mousePosition.x);
        }

        private void ShotLogic()
        {
            if (isPreparationShootingState == false && Input.GetKey(KeyCode.Mouse0))
            {
                isPreparationShootingState = true;
            }
            if (isPreparationShootingState)
            {
                timer += Time.deltaTime;
            }
            if (isPreparationShootingState && Input.GetKey(KeyCode.Mouse0) == false)
            {
                Debug.Log(forceShot);
                isPreparationShootingState = false;
                forceShot = Mathf.InverseLerp(0, timeToMaxForce, timer) * maxForce;
                CmdShot(forceShot);
                timer = 0;
            }
        }

        private void AddScoreForPlayer(int indexPlayer)
        {
            if (indexPlayer == PlayerIndex)
            {
                PlayerScore++;
            }
        }

        [Command]
        private void ExitGame()
        {
            for (int i = 0; i < balls.Count; i++)
            {
                NetworkServer.Destroy(balls[i].gameObject);
            }
            balls.Clear();
            game.PlayerExitToLobby(PlayerIndex);
            NetworkServer.Destroy(this.gameObject);
        }

        #region Start & Stop Callbacks

        /// <summary>
        /// This is invoked for NetworkBehaviour objects when they become active on the server.
        /// <para>This could be triggered by NetworkServer.Listen() for objects in the scene, or by NetworkServer.Spawn() for objects that are dynamically created.</para>
        /// <para>This will be called for objects on a "host" as well as for object on a dedicated server.</para>
        /// </summary>
        public override void OnStartServer() { }

        /// <summary>
        /// Invoked on the server when the object is unspawned
        /// <para>Useful for saving object data in persistent storage</para>
        /// </summary>
        public override void OnStopServer() { }

        /// <summary>
        /// Called on every NetworkBehaviour when it is activated on a client.
        /// <para>Objects on the host have this function called, as there is a local client on the host. The values of SyncVars on object are guaranteed to be initialized correctly with the latest state from the server when this function is called on the client.</para>
        /// </summary>
        public override void OnStartClient() { }

        /// <summary>
        /// This is invoked on clients when the server has caused this object to be destroyed.
        /// <para>This can be used as a hook to invoke effects or do client specific cleanup.</para>
        /// </summary>
        public override void OnStopClient() { }

        /// <summary>
        /// Called when the local player object has been set up.
        /// <para>This happens after OnStartClient(), as it is triggered by an ownership message from the server. This is an appropriate place to activate components or functionality that should only be active for the local player, such as cameras and input.</para>
        /// </summary>
        public override void OnStartLocalPlayer() { }

        /// <summary>
        /// Called when the local player object is being stopped.
        /// <para>This happens before OnStopClient(), as it may be triggered by an ownership message from the server, or because the player object is being destroyed. This is an appropriate place to deactivate components or functionality that should only be active for the local player, such as cameras and input.</para>
        /// </summary>
        public override void OnStopLocalPlayer() { }

        /// <summary>
        /// This is invoked on behaviours that have authority, based on context and <see cref="NetworkIdentity.hasAuthority">NetworkIdentity.hasAuthority</see>.
        /// <para>This is called after <see cref="OnStartServer">OnStartServer</see> and before <see cref="OnStartClient">OnStartClient.</see></para>
        /// <para>When <see cref="NetworkIdentity.AssignClientAuthority">AssignClientAuthority</see> is called on the server, this will be called on the client that owns the object. When an object is spawned with <see cref="NetworkServer.Spawn">NetworkServer.Spawn</see> with a NetworkConnectionToClient parameter included, this will be called on the client that owns the object.</para>
        /// </summary>
        public override void OnStartAuthority() { }

        /// <summary>
        /// This is invoked on behaviours when authority is removed.
        /// <para>When NetworkIdentity.RemoveClientAuthority is called on the server, this will be called on the client that owns the object.</para>
        /// </summary>
        public override void OnStopAuthority() { }

        #endregion

        #region SyncVar Hooks

        public virtual void PlayerIndexChanged(int oldIndex, int newIndex) => OnPlayerIndexChangedEvent?.Invoke(oldIndex, newIndex);

        public virtual void PlayerColorChanged(int oldColor, int newColor) { OnPlayerColorChangedEvent?.Invoke(oldColor, newColor); scoreUI.color = PlayerColor.ToColor(); }

        public virtual void PlayerScoreChanged(int oldScore, int newScore) { score.text = newScore.ToString(); scoreUI.text = score.text; }

        #endregion
    }
}