using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System;

namespace TestBallCannonGoalMirror
{
    public class NetLobbyPlayer : NetworkBehaviour
    {
        #region Unity Callbacks
        [SyncVar(hook = nameof(IndexChanged))]
        public int playerIndex;
        [SyncVar(hook = nameof(IndexGameChanged))]
        public int GameIndex;
        private NetworkLobbyManager networkLobbyManager;

        protected override void OnValidate()
        {
            base.OnValidate();
        }

        void Start()
        {
            if (NetworkManager.singleton is NetworkLobbyManager lobby)
            {
                networkLobbyManager = lobby;
                if (lobby.dontDestroyOnLoad)
                    DontDestroyOnLoad(gameObject);

                if (NetworkServer.active)
                    playerIndex = lobby.NextPlayerIndex;

                PlayersList.RegistrationPlayer(this, isLocalPlayer);
            }
            else Debug.LogError("NetLobbyPlayer could not find a NetworkLobbyManager. The NetLobbyPlayer requires a NetworkLobbyManager object to function. Make sure that there is one in the scene.");

        }
        #endregion

        [Command]
        public void CmdCreateGame()
        {
            GameObject newGame = Instantiate(networkLobbyManager.gamePrefab.gameObject, Vector3.zero, Quaternion.identity);
            NetworkServer.Spawn(newGame);
        }

        [Command]
        public void CmdSetGame(int indexGame)
        {
            GameIndex = indexGame;
        }

        private void OnDestroy()
        {
            PlayersList.RemovePlayer(this, isLocalPlayer);
        }

        #region Start & Stop Callbacks

        /// <summary>
        /// This is invoked for NetworkBehaviour objects when they become active on the server.
        /// <para>This could be triggered by NetworkServer.Listen() for objects in the scene, or by NetworkServer.Spawn() for objects that are dynamically created.</para>
        /// <para>This will be called for objects on a "host" as well as for object on a dedicated server.</para>
        /// </summary>
        public override void OnStartServer() { }

        /// <summary>
        /// Invoked on the server when the object is unspawned
        /// <para>Useful for saving object data in persistent storage</para>
        /// </summary>
        public override void OnStopServer() { }

 

        /// <summary>
        /// Called on every NetworkBehaviour when it is activated on a client.
        /// <para>Objects on the host have this function called, as there is a local client on the host. The values of SyncVars on object are guaranteed to be initialized correctly with the latest state from the server when this function is called on the client.</para>
        /// </summary>
        public override void OnStartClient() { }

        /// <summary>
        /// This is invoked on clients when the server has caused this object to be destroyed.
        /// <para>This can be used as a hook to invoke effects or do client specific cleanup.</para>
        /// </summary>
        public override void OnStopClient() { }

        /// <summary>
        /// Called when the local player object has been set up.
        /// <para>This happens after OnStartClient(), as it is triggered by an ownership message from the server. This is an appropriate place to activate components or functionality that should only be active for the local player, such as cameras and input.</para>
        /// </summary>
        public override void OnStartLocalPlayer() { }

        /// <summary>
        /// Called when the local player object is being stopped.
        /// <para>This happens before OnStopClient(), as it may be triggered by an ownership message from the server, or because the player object is being destroyed. This is an appropriate place to deactivate components or functionality that should only be active for the local player, such as cameras and input.</para>
        /// </summary>
        public override void OnStopLocalPlayer() { }

        /// <summary>
        /// This is invoked on behaviours that have authority, based on context and <see cref="NetworkIdentity.hasAuthority">NetworkIdentity.hasAuthority</see>.
        /// <para>This is called after <see cref="OnStartServer">OnStartServer</see> and before <see cref="OnStartClient">OnStartClient.</see></para>
        /// <para>When <see cref="NetworkIdentity.AssignClientAuthority">AssignClientAuthority</see> is called on the server, this will be called on the client that owns the object. When an object is spawned with <see cref="NetworkServer.Spawn">NetworkServer.Spawn</see> with a NetworkConnectionToClient parameter included, this will be called on the client that owns the object.</para>
        /// </summary>
        public override void OnStartAuthority() { }

        /// <summary>
        /// This is invoked on behaviours when authority is removed.
        /// <para>When NetworkIdentity.RemoveClientAuthority is called on the server, this will be called on the client that owns the object.</para>
        /// </summary>
        public override void OnStopAuthority() { }

        #endregion

        #region SyncVar Hooks

        public virtual void IndexChanged(int oldIndex, int newIndex) { }

        public virtual void IndexGameChanged(int oldIndex, int newIndex) { }
        public virtual void ColorPlayerChanged(int oldColor, int newColor) { }

        public virtual void ReadyStateChanged(bool oldReadyState, bool newReadyState) { }

        #endregion
    }
}