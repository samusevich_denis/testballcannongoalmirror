using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestBallCannonGoalMirror
{
    public class UIPlayerList : MonoBehaviour
    {
        public event Action<int> OnLocalPlayerChangeColorEvent;
        public int NumberLocalPlayer { get; private set; }
        public UIPlayer LocalPlayer { get; private set; }
        [SerializeField] private List<UIPlayer> uiPlayers = new List<UIPlayer>();
        [SerializeField] private Transform content;

        private void Start()
        {
            for (int i = 0; i < uiPlayers.Count; i++)
            {
                uiPlayers[i].OnChangeColor += LocalPlayerChangeColor;
            }
        }

        private void OnDestroy()
        {
            for (int i = 0; i < uiPlayers.Count; i++)
            {
                if (uiPlayers[i])
                {
                    uiPlayers[i].OnChangeColor -= LocalPlayerChangeColor;
                }
            }
        }

        public void Clear()
        {
            for (int i = 0; i < uiPlayers.Count; i++)
            {
                uiPlayers[i].SetName(0);
                uiPlayers[i].SetColor(0);
                uiPlayers[i].SetInteractableColor(false);
            }
        }

        public void SetPlayerColor(int playerListIndex, int newColor)
        {
            uiPlayers[playerListIndex].SetColor(newColor);
        }

        public void SetPlayerIndex(int playerListIndex, int newIndex)
        {
            uiPlayers[playerListIndex].SetName(newIndex);
        }       

        public void SetInteractableColorButton(int playerListIndex, bool isLocalPlayer)
        {
            if (isLocalPlayer)
            {
                NumberLocalPlayer = playerListIndex;
                LocalPlayer = uiPlayers[NumberLocalPlayer];
            }
            uiPlayers[playerListIndex].SetInteractableColor(isLocalPlayer);
        }

        private void LocalPlayerChangeColor(UIPlayer obj)
        {
            OnLocalPlayerChangeColorEvent?.Invoke(obj.Index);
        }

        public void SetInteractableChangeColorForLocalPlayer(bool value)
        {
            uiPlayers[NumberLocalPlayer].SetInteractableColor(value);
        }
    }
}