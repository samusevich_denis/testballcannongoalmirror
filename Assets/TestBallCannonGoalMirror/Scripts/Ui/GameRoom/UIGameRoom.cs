using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace TestBallCannonGoalMirror
{
    public class UIGameRoom : MonoBehaviour
    {
        [SerializeField] private UIPlayerList uiPlayerList;
        [SerializeField] private Button exitToLobby;
        [SerializeField] private Button playerReady;
        [SerializeField] private TextMeshProUGUI textReadyButton;
        [SerializeField] private GameObject prevPanel;
        private NetGame currentGame;
        private bool stateReady;

        private void Start()
        {
            exitToLobby.onClick.AddListener(ExitToLobby);
            playerReady.onClick.AddListener(PlayerReadyStateChange);
        }

        public void ShowPlayerList(NetGame game)
        {
            currentGame = game;
            SetPlayerData(0, currentGame.Player_00, currentGame.Player_00_Color, currentGame.Player_00 == PlayersList.LocalNetLobbyPlayer.playerIndex);
            SetPlayerData(1, currentGame.Player_01, currentGame.Player_01_Color, currentGame.Player_01 == PlayersList.LocalNetLobbyPlayer.playerIndex);
            SetPlayerData(2, currentGame.Player_02, currentGame.Player_02_Color, currentGame.Player_02 == PlayersList.LocalNetLobbyPlayer.playerIndex);
            SetPlayerData(3, currentGame.Player_03, currentGame.Player_03_Color, currentGame.Player_03 == PlayersList.LocalNetLobbyPlayer.playerIndex);
            uiPlayerList.OnLocalPlayerChangeColorEvent += LocalPlayerChangeColor;
            currentGame.OnPlayerIndexChangedEvent += OnPlayerIndexChanged;
            currentGame.OnPlayerColorChangedEvent += OnPlayerColorChanged;
            currentGame.OnPlayerReadyStateChangedEvent += OnPlayerReadyStateChanged;
            currentGame.OnGameStateChangedEvent += OnGameStateChanged;
            void SetPlayerData(int playerNumberInList, int indexPlayer,int indexColor, bool isLocalPlayer)
            {
                uiPlayerList.SetPlayerIndex(playerNumberInList, indexPlayer);
                uiPlayerList.SetPlayerColor(playerNumberInList, indexColor);
                uiPlayerList.SetInteractableColorButton(playerNumberInList, isLocalPlayer);
            }
            stateReady = false;
        }

        private void LocalPlayerChangeColor(int playerIndex)
        {
            currentGame.ChangePlayerColor(playerIndex);
        }

        private void OnPlayerColorChanged(int playerNumberInList, int oldColor, int newColor)
        {
            uiPlayerList.SetPlayerColor(playerNumberInList, newColor);
        }

        private void OnPlayerIndexChanged(int playerNumberInList, int oldIndex, int newIndex)
        {
            uiPlayerList.SetPlayerIndex(playerNumberInList, newIndex);
            uiPlayerList.SetInteractableColorButton(playerNumberInList, newIndex == PlayersList.LocalNetLobbyPlayer.playerIndex);
        }

        private void OnPlayerReadyStateChanged(int playerNumberInList, bool oldReadyState, bool newReadyState)
        {
            if(playerNumberInList == uiPlayerList.NumberLocalPlayer)
            {
                stateReady = newReadyState;
                exitToLobby.interactable = !newReadyState;
                playerReady.interactable = true;
                textReadyButton.text = newReadyState ? "No Ready" : "Ready";
                uiPlayerList.SetInteractableChangeColorForLocalPlayer(!newReadyState);
            }
        }

        private void PlayerReadyStateChange()
        {
            currentGame.CmdSetPlayerReadyState(PlayersList.LocalNetLobbyPlayer.playerIndex, !stateReady);
            if (!stateReady)
            {
                exitToLobby.interactable = false;
                playerReady.interactable = false;
                uiPlayerList.SetInteractableChangeColorForLocalPlayer(false);
            }
            else
            {
                playerReady.interactable = false;
            }
        }

        public void ExitToLobby()
        {
            uiPlayerList.OnLocalPlayerChangeColorEvent -= LocalPlayerChangeColor;
            currentGame.OnPlayerIndexChangedEvent -= OnPlayerIndexChanged;
            currentGame.OnPlayerColorChangedEvent -= OnPlayerColorChanged;
            currentGame.OnPlayerReadyStateChangedEvent -= OnPlayerReadyStateChanged;
            currentGame.OnGameStateChangedEvent -= OnGameStateChanged;
            uiPlayerList.Clear();
            currentGame.CmdRemovePlayer(PlayersList.LocalNetLobbyPlayer.playerIndex);
            currentGame = null;
            prevPanel.SetActive(true);
            gameObject.SetActive(false);
        }

        private void OnGameStateChanged(bool oldState, bool newState)
        {
            if (newState)
            {
                gameObject.SetActive(false);
                currentGame.OnPlayerExitToLobby += PlayerExitToLobby;
            }
        }

        private void PlayerExitToLobby(int playerIndex)
        {
            if (playerIndex == PlayersList.LocalNetLobbyPlayer.playerIndex)
            {
                currentGame.OnPlayerExitToLobby -= PlayerExitToLobby;
                ExitToLobby();
            }
        }
    }
}