using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using TestBallCannonGoalMirror.Extension;

namespace TestBallCannonGoalMirror
{
    public class UIPlayer : MonoBehaviour
    {
        public event Action<UIPlayer> OnChangeColor;
        public int Index { get; private set; }
        [SerializeField] private Image imageColor;
        [SerializeField] private TextMeshProUGUI namePlayer;
        [SerializeField] private Button buttonColor;

        private void Start()
        {
            buttonColor.onClick.AddListener(OnButtonClick);
        }

        private void OnDestroy()
        {
            if (buttonColor)
            {
                buttonColor.onClick.RemoveListener(OnButtonClick);
            }
        }

        public void SetName(int index)
        {
            if (index == 0)
            {
                Index = 0;
                namePlayer.text = $"Empty";
            }
            else
            {
                Index = index;
                namePlayer.text = $"Player_{index}";
            }
        }

        public void SetInteractableColor(bool value)
        {
            buttonColor.interactable = value;
        }

        private void OnButtonClick()
        {
            OnChangeColor?.Invoke(this);
        }

        public void SetColor(int newColor)
        {
            if (newColor == 0)
            {
                imageColor.color = Color.white;
            }
            else
            {
                imageColor.color = newColor.ToColor();;
            }
        }
    }
}