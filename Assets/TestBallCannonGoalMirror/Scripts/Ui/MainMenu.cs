using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TestBallCannonGoalMirror
{
    public class MainMenu : MonoBehaviour
    {
        [SerializeField] private Button startHostLobby;
        [SerializeField] private Button enterTolobby;
        [SerializeField] private Button exitGame;
        [SerializeField] private NetworkLobbyManager networkLobby;
        [SerializeField] private GameObject nextPanel;

        private void Start()
        {
            startHostLobby.onClick.AddListener(StartHost);
            enterTolobby.onClick.AddListener(EnterTolobby);
            exitGame.onClick.AddListener(ExitGame);
        }

        private void StartHost()
        {
            networkLobby.StartHost();
            if (networkLobby.isNetworkActive)
            {
                nextPanel.SetActive(true);
                gameObject.SetActive(false);
            }
        }

        private void EnterTolobby()
        {
            networkLobby.StartClient();
            if (networkLobby.isNetworkActive)
            {
                nextPanel.SetActive(true);
                gameObject.SetActive(false);
            }
        }

        private void ExitGame()
        {
            Application.Quit();
        }
    }
}