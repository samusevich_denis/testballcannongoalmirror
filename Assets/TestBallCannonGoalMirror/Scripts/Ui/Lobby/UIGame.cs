using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.UI;

namespace TestBallCannonGoalMirror
{
    public class UIGame : MonoBehaviour
    {
        public event Action<UIGame> OnClickGameEnterEvent;
        public int Index { get; private set; }

        [SerializeField] private TextMeshProUGUI textName;
        [SerializeField] private Button button;

        private void Start()
        {
            button.onClick.AddListener(OnButtonClick);
        }

        private void OnDestroy()
        {
            if (button)
            {
                button.onClick.RemoveListener(OnButtonClick);
            }
        }

        public void SetName(int index)
        {
            Index = index;
            textName.text = $"Game_{index}";
        }

        public void SetInteractable(bool value)
        {
            button.interactable = value;
        }

        private void OnButtonClick()
        {
            OnClickGameEnterEvent?.Invoke(this);
        }
    }
}