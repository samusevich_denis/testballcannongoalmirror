using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;
using System;

namespace TestBallCannonGoalMirror
{
    public class UILobby : MonoBehaviour
    {
        [SerializeField] private UIGameList uIGameList;
        [SerializeField] private Button createGameButton;
        [SerializeField] private Button backButton;
        [SerializeField] private NetworkLobbyManager networkLobby;
        [SerializeField] private GameObject prevPanel;
        [SerializeField] private UIGameRoom nextPanel;

        private void Start()
        {
            createGameButton.onClick.AddListener(CreateGame);
            backButton.onClick.AddListener(BackToMenu);
            uIGameList.OnPlayerEnterGame += PlayerEnterGame;
        }

        private void OnDestroy()
        {
            if (createGameButton)
            {
                createGameButton.onClick.RemoveListener(CreateGame);
            }
            if (uIGameList)
            {
                uIGameList.OnPlayerEnterGame += PlayerEnterGame;
            }
            if (backButton)
            {
                backButton.onClick.RemoveListener(BackToMenu);
            }
        }

        private void CreateGame()
        {
            PlayersList.LocalNetLobbyPlayer.CmdCreateGame();
            GamesList.OnCreateGameEvent += PlayerEnterToNewGame;
            gameObject.SetActive(false);
            nextPanel.gameObject.SetActive(true);
        }

        private void PlayerEnterToNewGame(NetGame newGame) 
        {
            GamesList.OnCreateGameEvent -= PlayerEnterToNewGame;
            nextPanel.ShowPlayerList(newGame);
            PlayersList.LocalNetLobbyPlayer.CmdSetGame(newGame.GameIndex);
            newGame.CmdSetPlayer(PlayersList.LocalNetLobbyPlayer.playerIndex);
        }

        private void PlayerEnterGame(int indexGame)
        {
            var game = GamesList.GetNetGameAnIndex(indexGame);
            if (game.IsCanEnterGame)
            {
                nextPanel.ShowPlayerList(game);
                PlayersList.LocalNetLobbyPlayer.CmdSetGame(indexGame);
                GamesList.GetNetGameAnIndex(indexGame).CmdSetPlayer(PlayersList.LocalNetLobbyPlayer.playerIndex);
                gameObject.SetActive(false);
                nextPanel.gameObject.SetActive(true);
            }
        }

        private void BackToMenu()
        {
            if (NetworkServer.activeHost)
            {
                try
                {
                    networkLobby.StopHost();
                }
                catch
                {
                    if (NetworkClient.active)
                    {
                        networkLobby.StopClient();
                    }
                }
            }
            else if (NetworkClient.active)
            {
                networkLobby.StopClient();
            }
            gameObject.SetActive(false);
            prevPanel.SetActive(true);
        }
    }
}