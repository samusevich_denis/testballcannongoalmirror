using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestBallCannonGoalMirror
{
    public class UIGameList : MonoBehaviour
    {
        public event Action<int> OnPlayerEnterGame;
        [SerializeField] private UIGame prefab;
        [SerializeField] private Transform content;
        [SerializeField] private List<UIGame> games = new List<UIGame>();

        private void Start()
        {
            GamesList.OnCreateGameEvent += OnCreateGame;
            GamesList.OnRemoveGameEvent += OnRemoveGame;
        }

        private void OnDestroy()
        {
            GamesList.OnCreateGameEvent -= OnCreateGame;
            GamesList.OnRemoveGameEvent -= OnRemoveGame;
        }

        private void OnCreateGame(NetGame netGame)
        {
            var gameUI = Instantiate(prefab);
            gameUI.transform.SetParent(content);
            gameUI.SetName(netGame.GameIndex);
            gameUI.OnClickGameEnterEvent += EnterPlayerToGame;
            games.Add(gameUI);
            netGame.OnEnterGameBlockChangedEvent += SetInteractbleButton;
        }

        private void OnRemoveGame(NetGame netGame)
        {
            var index = games.FindIndex(g=>g.Index == netGame.GameIndex);
            if (index>-1)
            {
                games[index].OnClickGameEnterEvent -= EnterPlayerToGame;
                Destroy(games[index].gameObject);
                games.RemoveAt(index);
            }
            netGame.OnEnterGameBlockChangedEvent -= SetInteractbleButton;
        }

        private void EnterPlayerToGame(UIGame obj)
        {
            OnPlayerEnterGame?.Invoke(obj.Index);
        }

        private void SetInteractbleButton(int gameIndex, bool oldValue, bool newValue)
        {
            var index = games.FindIndex(g => g.Index == gameIndex);
            if (index > -1)
            {
                games[index].SetInteractable(newValue);
            }
        }
    }
}