using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestBallCannonGoalMirror
{
    public class GoalTrecker : MonoBehaviour
    {
        [SerializeField] private NetPlayer netPlayer;
        [SerializeField] private string tagBall;
        [SerializeField] private BoxCollider boxCollider;

        private void Start()
        {
            if (!netPlayer.isServer)
            {
                boxCollider.enabled = false;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag(tagBall))
            {
                netPlayer.DestroyBall(other.transform.parent.gameObject);
            }
        }
    }
}