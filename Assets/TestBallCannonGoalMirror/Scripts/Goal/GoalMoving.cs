using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestBallCannonGoalMirror
{
    public class GoalMoving : MonoBehaviour
    {
        [SerializeField] private NetPlayer netPlayer;
        [SerializeField] private Vector3 minLocalPosition;
        [SerializeField] private Vector3 maxLocalPosition;
        [SerializeField] private float speedTime;

        private void Update()
        {
            if (netPlayer.isServer)
            {
                transform.localPosition = Vector3.Lerp(minLocalPosition, maxLocalPosition, (Mathf.Sin(Time.time * speedTime) + 1) / 2);
            }
        }
    }
}