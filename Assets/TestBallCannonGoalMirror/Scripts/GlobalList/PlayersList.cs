using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestBallCannonGoalMirror
{
    public static class PlayersList 
    {
        public static event Action<NetLobbyPlayer> AddPlayerToLobbyEvent;
        public static event Action<NetLobbyPlayer> RemovePlayerToLobbyEvent;
        public static NetLobbyPlayer LocalNetLobbyPlayer { get; private set; }
        private static List<NetLobbyPlayer> playerList = new List<NetLobbyPlayer>();

        public static void RegistrationPlayer(NetLobbyPlayer netLobbyPlayer, bool isLocalPlayer)
        {
            playerList.Add(netLobbyPlayer);
            if (isLocalPlayer)
            {
                LocalNetLobbyPlayer = netLobbyPlayer;
            }
            AddPlayerToLobbyEvent?.Invoke(netLobbyPlayer);
        }

        public static void RemovePlayer(NetLobbyPlayer netLobbyPlayer, bool isLocalPlayer)
        {
            playerList.Remove(netLobbyPlayer);
            if (isLocalPlayer)
            {
                LocalNetLobbyPlayer = null;
            }
            RemovePlayerToLobbyEvent?.Invoke(netLobbyPlayer);
        }

        public static bool TryGetNetLobbyPlayerAnIndex(int index, out NetLobbyPlayer lobbyPlayer)
        {
            lobbyPlayer = null;
            for (int i = 0; i < playerList.Count; i++)
            {
                if (index == playerList[i].playerIndex)
                {
                    lobbyPlayer = playerList[i];
                    return true;
                }
            }
            return false;
        }
    }
}