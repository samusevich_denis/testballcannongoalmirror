using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestBallCannonGoalMirror
{
    public static class GamesList
    {
        public static event Action<NetGame> OnRemoveGameEvent;
        public static event Action<NetGame> OnCreateGameEvent;
        private static List<NetGame> gameList = new List<NetGame>();

        public static void AddNetGame(NetGame netGame)
        {
            gameList.Add(netGame);
            OnCreateGameEvent?.Invoke(netGame);
        }

        public static void RemoveNetGame(NetGame netGame)
        {
            gameList.Remove(netGame);
            OnRemoveGameEvent?.Invoke(netGame);
        }

        public static NetGame GetNetGameAnIndex(int index)
        {
            for (int i = 0; i < gameList.Count; i++)
            {
                if (index == gameList[i].GameIndex)
                {
                    return gameList[i];
                }
            }
            return null;
        }
    }
}