using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestBallCannonGoalMirror
{
    public class CannonRotating : MonoBehaviour
    {
        public Vector3 defaultPosition { get; private set; }
        [SerializeField] private float rotateSpeed;
        [SerializeField] private Camera playerCamera;
        [Header("axis Y")]
        [SerializeField] private Transform axisY;
        [SerializeField] private AngleСalculation angleСalculationY;
        private Vector3 currentLocalEulerAnglesY;
        private int pixelHeight;
        [Header("axis X")]
        [SerializeField] private Transform axisX;
        [SerializeField] private AngleСalculation angleСalculationX;
        private Vector3 currentLocalEulerAnglesX;
        private int pixelWidth;

        private void Start()
        {
            pixelHeight = playerCamera.pixelHeight;
            pixelWidth = playerCamera.pixelWidth;
            defaultPosition = new Vector3( pixelWidth * 0.5f, pixelHeight * 0.5f);
        }

        public void RotateAroundY(float position)
        {
            currentLocalEulerAnglesY.y = angleСalculationY.Сalculation(0, pixelWidth, position);
            axisY.localEulerAngles = currentLocalEulerAnglesY;
        }

        public void RotateAroundX(float position)
        {
            currentLocalEulerAnglesX.x = angleСalculationX.Сalculation(0, pixelHeight, position);
            axisX.localEulerAngles = currentLocalEulerAnglesX;
        }

        [Serializable]
        public class AngleСalculation
        {
            public float maxAngle;
            public float minAngle;
            private float angleInterpolation;

            public float Сalculation(float minValue, float maxValue, float currentValue)
            {
                angleInterpolation = Mathf.InverseLerp(minValue, maxValue, currentValue );
                return Mathf.Lerp(minAngle, maxAngle, angleInterpolation);
            }
        }
    }
}