using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TestBallCannonGoalMirror.Extension;
using TMPro;

namespace TestBallCannonGoalMirror
{
    public class CannonColor : MonoBehaviour
    {
        public Material MaterialCannon { get; private set; }
        [SerializeField] private string colorPropertyName;
        [SerializeField] private NetPlayer netPlayer;
        [SerializeField] private MeshRenderer meshRenderer;
        [SerializeField] private MeshRenderer[] goal;
        [SerializeField] private TextMeshPro score;
        private int idColorProperty;
        private Color color;

        private void Start()
        {
            idColorProperty = Shader.PropertyToID(colorPropertyName);
            MaterialCannon = meshRenderer.material;
            for (int i = 0; i < goal.Length; i++)
            {
                goal[i].material = MaterialCannon;
            }
            color = netPlayer.PlayerColor.ToColor();
            MaterialCannon.SetColor(idColorProperty, color);
            score.color = color;
            netPlayer.OnPlayerColorChangedEvent += ChangeColor;
        }

        private void OnDestroy()
        {
            if (netPlayer)
            {
                netPlayer.OnPlayerColorChangedEvent -= ChangeColor;
            }
        }

        private void ChangeColor(int oldColor, int newColor)
        {
            color = newColor.ToColor();
            MaterialCannon.SetColor(idColorProperty, color);
            score.color = color;
        }
    }
}