using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestBallCannonGoalMirror.Extension
{
    public static class ListBallExtension
    {
        public static Ball GetFreeBall(this List<Ball> balls)
        {
            Ball ball;
            for (int i = 0; i < balls.Count; i++)
            {
                if (balls[i].gameObject.activeSelf == false)
                {
                    ball = balls[i];
                    balls.RemoveAt(i);
                    balls.Add(ball);
                    return ball;
                }
            }
            ball = balls[0];
            balls.RemoveAt(0);
            balls.Add(ball);
            return ball;
        }
    }
}