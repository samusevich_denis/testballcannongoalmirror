using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestBallCannonGoalMirror.Extension
{
    public static class IntExtension
    {
        public static Color ToColor(this int idColor)
        {
            switch (idColor)
            {
                case 0:
                    return Color.white;
                case 1:
                    return Color.red;
                case 2:
                    return Color.green;
                case 3:
                    return Color.blue;
                case 4:
                    return Color.magenta;
                case 5:
                    return Color.cyan;
                case 6:
                    return Color.yellow;
                default:
                    return Color.black;
            }
        }
    }
}